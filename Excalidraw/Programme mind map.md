---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Step ^THXb7ezb

Workout ^mByCV3yt

Phase ^dzDnVYqV

Programme ^zH9d41lN

- Exercises
- Reps (Summary)
- Weight Lifted 
  (Summary)
- Intensity
- Cycle Status
  (Female only)
- 
 ^vsGdmH0m

Exercise  ^S9GHcm8r

- Exercise(s)
  - Superset N
- Substitution(s)
- Warm-up Sets
- Step Sets
- Step RPE
- Step Reps
- Modifiers
- Actual Sets
- Actual RPE
- Actual Reps
- Actual Weight
- Weight Lifted ^1hCNrkCw

- Name
- Description
- Video
- Owner ^xqqwsE1I

- Exercises
- Length (weeks)
- Reps
- Weight Lifted
  (Summary)
-  ^bz3IGV4v

- Phases
- Length (weeks)
- Reps
- Weight Lifted
  (Summary)
-  ^aO66xBq6

Programme Rough Data Models ^pKQjJ0LG

User ^FXxxGKcH

Creator ^VIw6T9u5

- Name
- Date of Birth
- Programmes
- Creator?
- Membership
-  ^MrozmeWZ

- Name
- Description
- Video
- Owner ^NGWGkSSj

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.16",
	"elements": [
		{
			"type": "rectangle",
			"version": 79,
			"versionNonce": 1838288043,
			"isDeleted": false,
			"id": "C-eu7lF1Fqh5OAYWUMLUy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -450.961669921875,
			"y": -45.43736267089844,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 120,
			"height": 93,
			"seed": 343511845,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "THXb7ezb"
				},
				{
					"id": "IUz9cxauY29q6wGf95MfY",
					"type": "arrow"
				}
			],
			"updated": 1704566501766,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 86,
			"versionNonce": 1977788293,
			"isDeleted": false,
			"id": "THXb7ezb",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -413.0916442871094,
			"y": -11.437362670898438,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 44.25994873046875,
			"height": 25,
			"seed": 1507491525,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704566501766,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Step",
			"rawText": "Step",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "C-eu7lF1Fqh5OAYWUMLUy",
			"originalText": "Step",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "rectangle",
			"version": 213,
			"versionNonce": 1264122091,
			"isDeleted": false,
			"id": "Jn_hgpQS-37OGaPYAoIlR",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -131.20941162109375,
			"y": -45.0823974609375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 120,
			"height": 93,
			"seed": 493538117,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "mByCV3yt"
				},
				{
					"id": "IUz9cxauY29q6wGf95MfY",
					"type": "arrow"
				},
				{
					"id": "VBSLlXHRjxkopXsg-Su8M",
					"type": "arrow"
				}
			],
			"updated": 1704566501122,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 232,
			"versionNonce": 1837080389,
			"isDeleted": false,
			"id": "mByCV3yt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -110.24937438964844,
			"y": -11.0823974609375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 78.07992553710938,
			"height": 25,
			"seed": 1255873189,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704566501122,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Workout",
			"rawText": "Workout",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "Jn_hgpQS-37OGaPYAoIlR",
			"originalText": "Workout",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "rectangle",
			"version": 124,
			"versionNonce": 1665180555,
			"isDeleted": false,
			"id": "xzI9uskY8e8QHWcGAtA0V",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 126.2381591796875,
			"y": -49.40362548828125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 120,
			"height": 93,
			"seed": 1795284133,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "dzDnVYqV"
				},
				{
					"id": "VBSLlXHRjxkopXsg-Su8M",
					"type": "arrow"
				},
				{
					"id": "DiiX_3UhnTZfMOVzd7N9J",
					"type": "arrow"
				}
			],
			"updated": 1704566501122,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 131,
			"versionNonce": 1765109413,
			"isDeleted": false,
			"id": "dzDnVYqV",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 157.1181869506836,
			"y": -15.40362548828125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 58.23994445800781,
			"height": 25,
			"seed": 1783169029,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704566501122,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Phase",
			"rawText": "Phase",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "xzI9uskY8e8QHWcGAtA0V",
			"originalText": "Phase",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "rectangle",
			"version": 103,
			"versionNonce": 35256875,
			"isDeleted": false,
			"id": "Fvn-lhbVyS6lUHIrterYr",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 383.0013427734375,
			"y": -49.656036376953125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 120,
			"height": 93,
			"seed": 601651269,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "zH9d41lN"
				},
				{
					"id": "DiiX_3UhnTZfMOVzd7N9J",
					"type": "arrow"
				}
			],
			"updated": 1704566501122,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 115,
			"versionNonce": 1077563909,
			"isDeleted": false,
			"id": "zH9d41lN",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 392.44139099121094,
			"y": -15.656036376953125,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 101.11990356445312,
			"height": 25,
			"seed": 1351559077,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704566501122,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Programme",
			"rawText": "Programme",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "Fvn-lhbVyS6lUHIrterYr",
			"originalText": "Programme",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "arrow",
			"version": 394,
			"versionNonce": 1090797515,
			"isDeleted": false,
			"id": "IUz9cxauY29q6wGf95MfY",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -316.8643493652344,
			"y": -3.944024122461962,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 179.42953491210938,
			"height": 1.0396130440032154,
			"seed": 1706400075,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1704567541416,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "C-eu7lF1Fqh5OAYWUMLUy",
				"gap": 14.097320556640625,
				"focus": -0.09770699923179224
			},
			"endBinding": {
				"elementId": "Jn_hgpQS-37OGaPYAoIlR",
				"gap": 6.22540283203125,
				"focus": 0.14483011631793236
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					179.42953491210938,
					-1.0396130440032154
				]
			]
		},
		{
			"type": "arrow",
			"version": 355,
			"versionNonce": 252250379,
			"isDeleted": false,
			"id": "VBSLlXHRjxkopXsg-Su8M",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 0.7123413085937216,
			"y": -11.437883964188961,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 118.38067626953128,
			"height": 1.3555006201903979,
			"seed": 61762571,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1704567541420,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "Jn_hgpQS-37OGaPYAoIlR",
				"gap": 11.921752929687472,
				"focus": -0.25498445711812306
			},
			"endBinding": {
				"elementId": "xzI9uskY8e8QHWcGAtA0V",
				"gap": 7.1451416015625,
				"focus": 0.2258797958254071
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					118.38067626953128,
					-1.3555006201903979
				]
			]
		},
		{
			"type": "arrow",
			"version": 386,
			"versionNonce": 1927053899,
			"isDeleted": false,
			"id": "DiiX_3UhnTZfMOVzd7N9J",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 261.3538818359375,
			"y": 1.0151520106745906,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 111.36016845703125,
			"height": 1.8859693490150224,
			"seed": 1207777989,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1704567541423,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "xzI9uskY8e8QHWcGAtA0V",
				"gap": 15.11572265625,
				"focus": 0.055699708582936464
			},
			"endBinding": {
				"elementId": "Fvn-lhbVyS6lUHIrterYr",
				"gap": 10.28729248046875,
				"focus": -0.15252766203396867
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					111.36016845703125,
					1.8859693490150224
				]
			]
		},
		{
			"type": "text",
			"version": 195,
			"versionNonce": 585548069,
			"isDeleted": false,
			"id": "vsGdmH0m",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -173.51983642578125,
			"y": 81.03219604492188,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 206.0181121826172,
			"height": 274.9336853027343,
			"seed": 842431429,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704567425747,
			"link": null,
			"locked": false,
			"fontSize": 24.438549804687494,
			"fontFamily": 1,
			"text": "- Exercises\n- Reps (Summary)\n- Weight Lifted \n  (Summary)\n- Intensity\n- Cycle Status\n  (Female only)\n- \n",
			"rawText": "- Exercises\n- Reps (Summary)\n- Weight Lifted \n  (Summary)\n- Intensity\n- Cycle Status\n  (Female only)\n- \n",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "- Exercises\n- Reps (Summary)\n- Weight Lifted \n  (Summary)\n- Intensity\n- Cycle Status\n  (Female only)\n- \n",
			"lineHeight": 1.25,
			"baseline": 266
		},
		{
			"type": "rectangle",
			"version": 175,
			"versionNonce": 220025989,
			"isDeleted": false,
			"id": "kbEYZw4XJHr6FAOjQcW9j",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -763.2391357421875,
			"y": -43.156476739853076,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 120,
			"height": 93,
			"seed": 1611409547,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "S9GHcm8r"
				}
			],
			"updated": 1704567519788,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 195,
			"versionNonce": 1567875659,
			"isDeleted": false,
			"id": "S9GHcm8r",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -748.4790954589844,
			"y": -9.156476739853076,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 90.47991943359375,
			"height": 25,
			"seed": 1160568619,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704567519788,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Exercise ",
			"rawText": "Exercise ",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "kbEYZw4XJHr6FAOjQcW9j",
			"originalText": "Exercise ",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 401,
			"versionNonce": 1788113765,
			"isDeleted": false,
			"id": "1hCNrkCw",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -473.5416488647461,
			"y": 82.90022277832031,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 199.25099182128906,
			"height": 397.12643432617176,
			"seed": 1779461771,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704567190740,
			"link": null,
			"locked": false,
			"fontSize": 24.438549804687494,
			"fontFamily": 1,
			"text": "- Exercise(s)\n  - Superset N\n- Substitution(s)\n- Warm-up Sets\n- Step Sets\n- Step RPE\n- Step Reps\n- Modifiers\n- Actual Sets\n- Actual RPE\n- Actual Reps\n- Actual Weight\n- Weight Lifted",
			"rawText": "- Exercise(s)\n  - Superset N\n- Substitution(s)\n- Warm-up Sets\n- Step Sets\n- Step RPE\n- Step Reps\n- Modifiers\n- Actual Sets\n- Actual RPE\n- Actual Reps\n- Actual Weight\n- Weight Lifted",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "- Exercise(s)\n  - Superset N\n- Substitution(s)\n- Warm-up Sets\n- Step Sets\n- Step RPE\n- Step Reps\n- Modifiers\n- Actual Sets\n- Actual RPE\n- Actual Reps\n- Actual Weight\n- Weight Lifted",
			"lineHeight": 1.25,
			"baseline": 388
		},
		{
			"type": "text",
			"version": 211,
			"versionNonce": 1037632773,
			"isDeleted": false,
			"id": "xqqwsE1I",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -807.656364440918,
			"y": 76.83601253974871,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 152.1988067626953,
			"height": 122.19274902343747,
			"seed": 1476913771,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704567533840,
			"link": null,
			"locked": false,
			"fontSize": 24.438549804687494,
			"fontFamily": 1,
			"text": "- Name\n- Description\n- Video\n- Owner",
			"rawText": "- Name\n- Description\n- Video\n- Owner",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "- Name\n- Description\n- Video\n- Owner",
			"lineHeight": 1.25,
			"baseline": 113
		},
		{
			"type": "text",
			"version": 248,
			"versionNonce": 858437707,
			"isDeleted": false,
			"id": "bz3IGV4v",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 100.0730972290039,
			"y": 71.71952819824219,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 197.1500244140625,
			"height": 183.2891235351562,
			"seed": 1025853029,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704566618857,
			"link": null,
			"locked": false,
			"fontSize": 24.438549804687494,
			"fontFamily": 1,
			"text": "- Exercises\n- Length (weeks)\n- Reps\n- Weight Lifted\n  (Summary)\n- ",
			"rawText": "- Exercises\n- Length (weeks)\n- Reps\n- Weight Lifted\n  (Summary)\n- ",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "- Exercises\n- Length (weeks)\n- Reps\n- Weight Lifted\n  (Summary)\n- ",
			"lineHeight": 1.25,
			"baseline": 174
		},
		{
			"type": "text",
			"version": 253,
			"versionNonce": 2037517509,
			"isDeleted": false,
			"id": "aO66xBq6",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 361.5788345336914,
			"y": 73.18589782714844,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 197.1500244140625,
			"height": 183.2891235351562,
			"seed": 1382614571,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704566656396,
			"link": null,
			"locked": false,
			"fontSize": 24.438549804687494,
			"fontFamily": 1,
			"text": "- Phases\n- Length (weeks)\n- Reps\n- Weight Lifted\n  (Summary)\n- ",
			"rawText": "- Phases\n- Length (weeks)\n- Reps\n- Weight Lifted\n  (Summary)\n- ",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "- Phases\n- Length (weeks)\n- Reps\n- Weight Lifted\n  (Summary)\n- ",
			"lineHeight": 1.25,
			"baseline": 174
		},
		{
			"id": "pKQjJ0LG",
			"type": "text",
			"x": -661.1170131521816,
			"y": -204.15936368370933,
			"width": 1171.514892578125,
			"height": 95.89539801079616,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 133403013,
			"version": 61,
			"versionNonce": 123420037,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1704567286890,
			"link": null,
			"locked": false,
			"text": "Programme Rough Data Models",
			"rawText": "Programme Rough Data Models",
			"fontSize": 76.71631840863692,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 68,
			"containerId": null,
			"originalText": "Programme Rough Data Models",
			"lineHeight": 1.25
		},
		{
			"type": "rectangle",
			"version": 259,
			"versionNonce": 1064885029,
			"isDeleted": false,
			"id": "R-q8RQD907uCa7AkEEXBt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -787.0428170512773,
			"y": 603.8821238969979,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 120,
			"height": 93,
			"seed": 2103162693,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "FXxxGKcH"
				}
			],
			"updated": 1704567527787,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 283,
			"versionNonce": 896636331,
			"isDeleted": false,
			"id": "FXxxGKcH",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -749.3827981303789,
			"y": 637.8821238969979,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 44.679962158203125,
			"height": 25,
			"seed": 1439598245,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704567527787,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "User",
			"rawText": "User",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "R-q8RQD907uCa7AkEEXBt",
			"originalText": "User",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "rectangle",
			"version": 257,
			"versionNonce": 488029515,
			"isDeleted": false,
			"id": "fqODQbKEFHOgiblCeoR21",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -345.7706503382947,
			"y": 607.5814892223822,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 120,
			"height": 93,
			"seed": 1738662949,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "VIw6T9u5"
				}
			],
			"updated": 1704567540337,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 284,
			"versionNonce": 1924583653,
			"isDeleted": false,
			"id": "VIw6T9u5",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -324.14061493790405,
			"y": 641.5814892223822,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 76.73992919921875,
			"height": 25,
			"seed": 1954289541,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704567540337,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Creator",
			"rawText": "Creator",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "fqODQbKEFHOgiblCeoR21",
			"originalText": "Creator",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 375,
			"versionNonce": 1299589675,
			"isDeleted": false,
			"id": "MrozmeWZ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -806.8746898171552,
			"y": 752.1138296064245,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 194.0962677001953,
			"height": 183.2891235351562,
			"seed": 1021058149,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704567593454,
			"link": null,
			"locked": false,
			"fontSize": 24.438549804687494,
			"fontFamily": 1,
			"text": "- Name\n- Date of Birth\n- Programmes\n- Creator?\n- Membership\n- ",
			"rawText": "- Name\n- Date of Birth\n- Programmes\n- Creator?\n- Membership\n- ",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "- Name\n- Date of Birth\n- Programmes\n- Creator?\n- Membership\n- ",
			"lineHeight": 1.25,
			"baseline": 174
		},
		{
			"type": "text",
			"version": 264,
			"versionNonce": 1335769739,
			"isDeleted": false,
			"id": "NGWGkSSj",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -362.51183674750814,
			"y": 741.6614487614705,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 152.1988067626953,
			"height": 122.19274902343747,
			"seed": 1339386949,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1704567543772,
			"link": null,
			"locked": false,
			"fontSize": 24.438549804687494,
			"fontFamily": 1,
			"text": "- Name\n- Description\n- Video\n- Owner",
			"rawText": "- Name\n- Description\n- Video\n- Owner",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "- Name\n- Description\n- Video\n- Owner",
			"lineHeight": 1.25,
			"baseline": 113
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 917.4827113821239,
		"scrollY": 236.1701918295005,
		"zoom": {
			"value": 0.5743238642811772
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%