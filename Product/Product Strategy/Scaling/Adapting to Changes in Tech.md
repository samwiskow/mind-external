Adapting to changes in tech is an example of [[Tech scaling]].

A common mistake product leaders make is not understanding the difference between tech scaling as a differentiator or enabler.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601505075703-M4+Product+Strategy.075.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601505075703-M4+Product+Strategy.075.jpeg)
Using tech to enable the value prop, on the other hand, is not about changing or improving the value proposition, but rather strengthening your ability to consistently deliver on this existing value proposition.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601505316347-M4+Product+Strategy.076.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601505316347-M4+Product+Strategy.076.jpeg)
Product leaders could think about this using the Diffusion of Innovation, which states that adoption of new innovation or product occurs in stages, rather than being simultaneously adopted by all players. This concept was coined by renowned sociologist Everett Rogers.

We can visualise these stages as a bell curve, in which technology is first slowly adapted, then gradually increases as it's adopted by a majority of users. Innovation adoption often follows this curve, due to time and friction related to becoming aware of that technology, deciding to adopt that technology, and building it.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601505362081-M4+Product+Strategy.078.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601505362081-M4+Product+Strategy.078.jpeg)

There are five key stages in the diffusion of innovation: Innovators, Early Adopters, Early Majority, Late Majority, and Laggards. This can be contrasted to the [[Pioneers, Settlers & Town Planners]] framework that is used when thinking about [[Wardley Maps Overview]]

Product leaders want to be innovators or early adopters for tech when it's used to drive product strategy. It is often advantageous for companies to leverage tech as a differentiator by being an early adopter, as early adoption allows you to use tech in novel ways to add to your value proposition.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601505694441-M4+Product+Strategy.084.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601505694441-M4+Product+Strategy.084.jpeg)
Leveraging tech as an enabler is more acceptable for later adoption, since the underlying tech often does not need to be differentiated.

Being an innovator or early adopter allows you to differentiate your product effectively. New tech adopted in a novel way could add significant value to your product strategy and could be a shared capability that helps you out-compete your peers.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601505745876-M4+Product+Strategy.087.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601505745876-M4+Product+Strategy.087.jpeg)
However, being an innovator or early adopter also results in increased risk, as new tech is often unproven and has a higher risk of failure than existing technology.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601505776581-M4+Product+Strategy.089.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601505776581-M4+Product+Strategy.089.jpeg)

However, when tech is used as an enabler, it's actually smarter for product leaders to be in the middle to the back end of the adoption curve and adopt at the early majority or later. Instead of being a differentiator, tech as an enabler involves leveraging tech that is proven and reliable.