User scaling involves identifying and managing unintended uses of your product. As you grow, it's inevitable that users will find a way to use your product in unintended ways. And there are three ways this can happen.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1602019562102-M4+Product+Strategy.425.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1602019562102-M4+Product+Strategy.425.jpeg)

The first is through value-added use cases. These are unintended use cases that actually can drive a business outcome or create an opportunity.

There's also under-served segments. These are specific user segments for whom the product is unintentionally not optimized.

And the third is bad behavior. These are use cases that have negative consequences if left unchecked.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1602019664551-M4+Product+Strategy.426.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1602019664551-M4+Product+Strategy.426.jpeg)