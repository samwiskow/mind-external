Build Vs Buy Vs Partner is an example of [[Tech scaling]].

How aligned is the buy solution's roadmap to our future needs? Specifically, even if the buy approach works in the near-term, will it continue to work into the future? Long-term viability impacts the decision, as well-maintained internally-built tools are often more adaptable to long-term changes.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601572406040-M4+Product+Strategy.142.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601572406040-M4+Product+Strategy.142.jpeg)

It's a product leader's job to understand which of these factors should be weighted most heavily in driving the decision.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601572424021-M4+Product+Strategy.143.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601572424021-M4+Product+Strategy.143.jpeg)

For the role of tech, often highly differentiated tech won't be ready for purchase, so building makes more sense.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601572458909-M4+Product+Strategy.144.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601572458909-M4+Product+Strategy.144.jpeg)

When many internal users will use an internal tool that is important for the organization, building a unique solution could be a more important factor.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601572475770-M4+Product+Strategy.145.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601572475770-M4+Product+Strategy.145.jpeg)

If cost effectiveness is the priority in building the tool or the team has limited budget following the approach with the lower cost to the organization could be ideal.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601572489197-M4+Product+Strategy.146.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601572489197-M4+Product+Strategy.146.jpeg)

When the long-term viability of a solution in terms of predictability is critical to the success of your product or a large part of the organization, teams should base their decision on predictability.