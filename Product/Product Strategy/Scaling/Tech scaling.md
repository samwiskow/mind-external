Tech scaling is about scaling your technology to incorporate new functionality. There are two primary reasons why it is critical for product leaders to manage tech scaling.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601504574483-M4+Product+Strategy.051.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601504574483-M4+Product+Strategy.051.jpeg)

The first is that underinvesting in tech scaling can lead to poor product performance, which often results in product market fit degradation.

This is because tech scaling often leads to a bad user experience, where the product underperforms in terms of performance or reliability.

We can see the importance of performance play out in the search engine wars between Google and Yahoo. Google had and continues to use a simple site that loads quickly, while Yahoo had a site with lots of content, like news, on the same place where the search engine is, leading to a slower interaction. Better performance was a primary reason that Google won over the long term.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601504665117-M4+Product+Strategy.055.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601504665117-M4+Product+Strategy.055.jpeg)

Reliability is how often actions accomplish what a user is expecting them to accomplish. High quality products are those with consistency, graceful error handling, smooth transitions, appropriate loading, and high feeling of responsiveness. High-quality products generally have bugs that do not significantly impact the user experience.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601504727743-M4+Product+Strategy.057.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601504727743-M4+Product+Strategy.057.jpeg)

The second reason why tech scaling is critical is that when product leaders don't put enough focus into tech scaling, they end up being limited in their ability to do growth, feature or PMF expansion work.

Tech Scaling includes things like:
- [[Adapting to Changes in Tech]]
- [[Managing Debt]]
- [[Build Vs Buy Vs Partner]]
- [[Modernistation]]