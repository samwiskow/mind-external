Managing Debt is an example of [[Tech scaling]].
## Factors that Lead to Debt

- Non-Roadmap Work
- Workarounds
- Deprecating Tech
- Unplanned Work

![[factors_that_lead_to_debt.png]]

## Prioritising Debt

A good framework for prioritising debt is the following matrix:

- Confidence
- Time
- Impact to User
- Sequence
- Accumulated Debt

![[prioritising_debt.png]]

And you can measure those categories in the way shown in the image below

![[prioritising_debt2.png]]

## Categorising Debt

There are three main categories of debt in products:

- Systemic Debt
- Extinction Event Debt
- Papercut Debt

![[categories_debt.png]]

Let's first look at systemic debt. This is when previous decisions result in the creation of more work at the organisational level.

![[systemic_debt_solve.png]]

Systemic debt should often be solved or remedied, as these are usually important problems with high confidence that a problem will occur in the near term. If the problem will prevent sequencing or reaching key milestones, then it should be solved. That is structurally addressing the problem and solving at a system level to ensure that the problem does not reoccur.

![[systemic_debt_remedy.png]]

However, in situations where the debt has minimal impact on sequencing, you can use your discretion on whether to solve it or just remedy it. Where remedying involves a fast solution that temporarily fixes the problem or finds a workaround.

Next, let's look at an extinction event.
![[extinction_event_debt.png]]

An extinction event is an unexpected event that could cause significant damage to your business. These are high priority because of high confidence it causes serious problems for the user in the near term. For these reasons, you should resolve the problems immediately. Given that the confidence time and impact to the user are all high, sequence and accumulated debt do not make a significant difference in how you should approach these types of events. These are already highly critical to solve and need to be done immediately.

Next, let's look at papercut debt.

![[papercut_debt.png]]

Papercuts have low confidence and are often are not time sensitive. These are problems that will not significantly harm the business and often occur sporadically. Product leaders should deprioritise these and solve on an ad hoc basis when your team has the bandwidth.