Modernisation is an example of [[Tech scaling]].

There are two situations where Modernisation typically makes sense.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601654125389-M4+Product+Strategy.243.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601654125389-M4+Product+Strategy.243.jpeg)

First, modernising might make sense when grouped with major updates to the product. These major updates should be those where a front-end or backend redesign allows for easier deployment or an improved experience over time.

Second, modernisation might make sense when it helps mitigate the risk of shifts in technology that leave your product vulnerable to disruption. If the tech used in your product is at risk of disruption, it often makes sense to go through one of these efforts.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601654151498-M4+Product+Strategy.248.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601654151498-M4+Product+Strategy.248.jpeg)

In the past decade or so, there have been four major shifts in tech that have led product leaders to modernise their products. First is a front-end shift away from Flash towards JavaScript and React. The second is the shift from desktop to mobile. The third is the shift from on-premise to cloud, and the fourth is the shift to using AI and machine learning within the core of our products.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601654219820-M4+Product+Strategy.251.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601654219820-M4+Product+Strategy.251.jpeg)

If you're going to go through a major monetisation effort, there are five success factors product leaders should leverage.

## Align with supporting insights

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601654312402-M4+Product+Strategy.256.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601654312402-M4+Product+Strategy.256.jpeg)

We can do this by looking at a few different sources of information. The first is tech data. Are there any indicators or signals that the product is underperforming technically?

The second is product data. Are there product KPIs suffering despite feature and growth work?

And then third is user research. Are there any indications that users aren't adopting or using the product due to some tactical shift?

## Defining Scope

The second major success factor in modernisation is defining scope and objectives of the redesign. We want to modernise with the future product roadmap in mind.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601654357138-M4+Product+Strategy.261.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601654357138-M4+Product+Strategy.261.jpeg)

Specifically, we want to define what success looks like based on two key objectives. The first is how the modernisation will help deploy more quickly or effectively, and the second, how will it help improve product KPIs?

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601655521213-M4+Product+Strategy.262.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601655521213-M4+Product+Strategy.262.jpeg)

Ultimately the redesign should create and capture value for both users and the product. Product leaders should specifically define which metrics they anticipate will be impacted. This helps guide the modernisation but also functions as a way to measure success after the modernisation.

## Deconstruct & Simplify

The third success factor of modernization is deconstructing and simplifying.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601655559631-M4+Product+Strategy.264.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601655559631-M4+Product+Strategy.264.jpeg)
After defining your objectives and analyzing your sources of insight, you should develop a plan that is broken into discreet user-centric iterations. One common mistake here is for product leaders to modernize their product and to launch an intermediate version that is not fully functional. Instead, product leaders should incrementally modernize their product in an iterative way in which each version is functional and built upon the previous version.

## Collaborate Cross Functionally 

Successful modernization efforts require collaboration and alignment across multiple teams. Too often marketing and product teams look at a redesign as an opportunity to remake the product with a new look, often thinking more about form rather than function. Product leaders should identify key stakeholders, align on modernization objectives and build together as a team to avoid this.

## Balance User Needs

The fifth success factor is balancing user needs.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601655863957-M4+Product+Strategy.278.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601655863957-M4+Product+Strategy.278.jpeg)

This specifically refers to the trade offs involved with engaging power users and acquiring new users. Power users often demand more features and functionality in products while new users prefer simpler user experiences and basic functionality. Over optimising for more features can lead to less acquisition while simplifying the product could lead power users to churn.