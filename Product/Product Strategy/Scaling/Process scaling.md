Process scaling is redesigning product development processes to increase their throughput, quality, or consistency.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601656651524-M4+Product+Strategy.293.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601656651524-M4+Product+Strategy.293.jpeg)

As organizations scale, the processes they need to run effectively evolve in several different ways. The first is that organizational complexity increases, and communicating and aligning efforts across teams becomes more challenging. This creates more potential for misunderstanding or misalignment, which often leads to processes breaking or no longer working as initially designed.

The second thing that happens as an organization scales is the user base grows. As a user base grows, you need to expand the capability of your existing processes to cover the changing needs of the users.

And then, the third thing is that the tech undergoes major changes. As your business improves or changes in tech, you need to adapt your processes to match these changes.

For a product team, some example processes could include collecting user feedback or how we design new features, how we do strategic planning, how we evaluate product analytics, how we run QA, how we ship products, how we onboard new hires, how we do budgeting, evaluating tools or reporting internally or externally.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601656764914-M4+Product+Strategy.311.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601656764914-M4+Product+Strategy.311.jpeg)
For most processes, at a certain scale a process just breaks entirely because it can no longer keep up with the needs of the organisation.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601657011975-M4+Product+Strategy.321.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601657011975-M4+Product+Strategy.321.jpeg)
For example, if you have one person running all hiring for your product team, that might work with a small organisation, but at a certain point, that one person won't be able to keep up with the scale required for a growing team.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601657028767-M4+Product+Strategy.322.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601657028767-M4+Product+Strategy.322.jpeg)
New processes are implemented with the intention of eventually creating more value and being set up to handle more scale, but they almost always start with a dip in value during an implementation period.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601657053277-M4+Product+Strategy.323.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601657053277-M4+Product+Strategy.323.jpeg)
When product leaders think too short term, they avoid making process improvements, not wanting to sacrifice the value of existing processes and not wanting to invest time and resources into implementing the new ones. However, this often leads to processes whose value doesn't scale with the organisation, hurting the output and the functionality of the product team and in extreme cases, breaking it altogether.

Process Scaling involves things like:

- [[Process Improvement Evaluation]]
- [[Value Stream Mapping]]