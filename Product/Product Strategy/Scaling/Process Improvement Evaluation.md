[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601657789587-M4+Product+Strategy.329.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601657789587-M4+Product+Strategy.329.jpeg)
Let's start with a simple example of a growing company with a messy database. Let's assume every quarter, it takes four junior PMs three weeks to go into the database, and wrangle it to put together a bunch of data and build visualisations needed for executive presentations in quarterly board meetings.

In order to decide what the best way to redesign a process is, product leaders need to apply some heuristics to think through what process improvement adds the most value for the least effort.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601657941760-M4+Product+Strategy.339.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601657941760-M4+Product+Strategy.339.jpeg)
Product leaders often make three mistakes when thinking through this decision of when and how to invest in scaling processes. First is defaulting to building customized solutions for labor intensive processes. Second is not accounting or planning for implementing process improvements. And third is over-investing in highly scalable processes.

Matt Greenberg, the former VP of Engineering at Credit Karma, says that product leaders need to first sense check their math. "Sense checking the math is about creating a simple conceptual model of how to compare different approaches to improving a process and seeing which create the most value with the least opportunity cost."

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601658179521-M4+Product+Strategy.344.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601658179521-M4+Product+Strategy.344.jpeg)
In this case the four junior PMs are taking three weeks every quarter. That means they are investing 48 weeks a year in this process.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601658196777-M4+Product+Strategy.345.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601658196777-M4+Product+Strategy.345.jpeg)
An engineering solution may take a team of four engineers 10 weeks to build out, making it a one-time investment of 40 weeks. However, that 40 weeks doesn't capture the opportunity cost. So what in a product roadmap gets slowed down by spending 40 engineering weeks on this initiative?

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601658223242-M4+Product+Strategy.346.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601658223242-M4+Product+Strategy.346.jpeg)
Losing 40 weeks of engineering time could be way more valuable than gaining 48 weeks of junior PM time if we include the opportunity cost.

The last problem product leaders face is over-investing in highly scalable processes.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601658523499-M4+Product+Strategy.366.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601658523499-M4+Product+Strategy.366.jpeg)
In many situations, product leaders want to come up with processes that pay compounding dividends with scale, and won't ever need to be improved further in the future, that perfect process. These types of investments typically have a very long payback period, with the hope that they will continue to last and scale for a long time.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601658537605-M4+Product+Strategy.367.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601658537605-M4+Product+Strategy.367.jpeg)
However, great product leaders don't look to create perfect long lasting processes and said they get good at iterating on processes based on the needs of the business.

As your product grows, organizational complexities grow and new processes needs are introduced and old processes become obsolete. It is better to lean into this evolution than a revolution and try to prevent coming up with that perfect process.

[![](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601658555391-M4+Product+Strategy.368.jpeg)](https://s3.amazonaws.com/reforge-uploads-prod/froala%2F1601658555391-M4+Product+Strategy.368.jpeg)
It's important to get into a rhythm of identifying process bottlenecks and approaching them with consistent incremental improvements. Experimenting with lightweight process improvements can often yield better results than large scale process redesigns.