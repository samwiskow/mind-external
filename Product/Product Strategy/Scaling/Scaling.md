Product leaders should think about scaling their products in three ways:

- [[Tech scaling]], which is scaling our technology to incorporate new functionality.
- [[Process scaling]], which is scaling your internal processes to handle the increased scope of work.
- [[User scaling]], or evolving your product to accommodate the different ways users interact with your product at scale.

![[scaling.png]]



Process Scaling includes things like:

User Scaling includes things like:
