The difficulties teams experience often fall into two categories: struggling with shared understanding, or falling into common traps.

## Common Traps
### Category 1: The team needs shared understanding

If you’re experiencing this, try:

- Surfacing beliefs    
- Connecting the North Star to your product vision    
- Understanding key value exchanges    
### Category 2: The team needs help avoiding common traps

 If you’re experiencing this, watch out for:  
- Jumping immediately to “Can we measure that?”  
- Focusing on the North Star Metric only, not the Inputs  
- Insisting you need more than one North Star Metric  
- Letting current dysfunction overwhelm your ability to improve

## JUMPING IMMEDIATELY TO “CAN WE MEASURE THAT?”

**Myth**: When you have a lot of uncertainty, you need a lot of data to tell you something useful.

**Fact**: If you have a lot of uncertainty now, you don’t need much data to reduce uncertainty significantly. When you have a lot of certainty already, then you need a lot of data to reduce uncertainty significantly. In other words—if you know almost nothing, almost anything will tell you something.

## FOCUSING ON THE NORTH STAR METRIC ONLY, NOT THE INPUTS

By design, the North Star Metric is not immediately actionable—the Inputs are where you apply actions. If you find yourself thinking only about the metric, revisit Inputs.

## INSISTING YOU NEED MORE THAN ONE NORTH STAR METRIC

Here’s an example. A large bank has dozens of consumer banking “products” (savings accounts, checking accounts, investment accounts, etc.). From a balance sheet and organizational chart perspective, it makes sense to call each of these things “products.” But do customers view these as distinct products? Or do they view the bank as a single product: a trustworthy partner in their quest for financial independence? In this case, a single North Star for all consumer banking products is usually appropriate.

## LETTING CURRENT DYSFUNCTION OVERWHELM YOUR ABILITY TO IMPROVE

- **Start where you are**. Get it all on the table. What is the current strategy, imperfections and all? How does all that prescriptive work map to that strategy? How does that A/B testing really map into customer satisfaction? Acknowledge imperfections in what you’re doing now, but don’t let them stop you from making progress. Just start using the language of the framework, socialize assumptions, and slowly chip away.

- **Start small**. Don’t try to do too much. Pick a product scope you have an ability to impact, even if it just represents a small percentage of your company’s business. Then build and test a North Star—the metric, the Inputs, the results they impact—for just that portion of your company or product portfolio.