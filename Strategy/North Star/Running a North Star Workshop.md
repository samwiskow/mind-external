#TODO

TODO: Sync workshop format

TODO: Async Workshop with sync pieces

For detail/reference - take a look at the [[North Star Playbook]] by amplitude.

### Getting Inputs Right

Inputs are the handful of factors that, together, produce the North Star Metric. They are as important to the North Star Framework as the metric is.

Just like the North Star Metric, the Inputs should have both a name and a definition.

The following fill-in-the-blank template can help you determine the Inputs to your metric:

**I believe that < NORTH STAR METRIC > is a function of < X, Y, and Z >. I also believe that there is some independence between X,Y, and Z (e.g. movements in one aren’t immediately felt in the others).**

Spend time editing and clarifying the concepts in your mind map so that the metric is at the heart of it and the contributing factors are all neatly named and defined at a high level, like this:

![[input_funel.png]]


Once you have your Inputs defined at a high level, you’ll find it much easier to tackle the challenge of defining an exact metrics for each Input.

![[input_funnel_2.png]]

## THE ROADMAP CHECK
In some cases, you can’t see the link between the roadmap item and the Input. This is a good signal that your set of inputs is missing some factor of your North Star—or that you’re working on something that isn’t actually valuable.

