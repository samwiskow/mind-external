Even with complex products, you can typically isolate a handful (3-6) of essential actions or events where customers derive value from the product. These key value exchanges demonstrate the true essence and intent of the product. If possible, they should be reflected in your North Star.

## EXAMPLES OF KEY VALUE EXCHANGES:

- The delivery arrives intact outside your apartment in under 30 minutes • Monthly account reconciliation was successful  
- A collaborator makes their first comment on your design  
- “Wow. This music recommendation is actually pretty decent.”
- Based on the logging, it looks like the SDK is working

