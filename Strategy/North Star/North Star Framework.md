#northstar #north #star #strategy #foundations #alignment #direction
## What is the North Star Framework?

The North Star Framework is a model for managing products by identifying a single, crucial metric (the North Star Metric) that, according to Sean Ellis, “best captures the core value that your product delivers to its customers.”

In addition to the metric, the North Star Framework includes a set of key Inputs that collectively act as factors that produce the metric. Product teams can directly influence these Inputs with their day-to-day work.

This combination of metric and Inputs serves three critical purposes in any company:

1. It helps prioritise and accelerate informed but decentralised decision-making. 2. It helps teams align and communicate.  
3. It enables teams to focus on impact and sustainable, product-led growth.

![[north_star_workshop_template.png]]

A Real-World Example: Burger King

Burger King’s digital team used the North Star Framework to define a North Star Metric called Digital Transactions Per User with three Inputs: new user activation, registration, and frequency. They then mobilised teams (called “squads” in Burger King’s organisation) to drive these Inputs, as illustrated in the following figure.
![[burger_king_north_star.png]]

The Burger King squads are able to trace the feature work that they prioritise through their development processes first to these Inputs, and then to their “Digital Transactions Per User North Star Metric. For example, one squad prioritised a “mobile order only coupons” initiative to drive the “frequency” Input—a factor in the metric.

## Defining a North Star

**Too Vague:**
Our North Star Metric tracks subscribers who share our content.

**A Better Choice:**
Our North Star Metric: Frequent Content Sharers (FCS). Definition: The number of unique subscribers who share an average of two or more articles per week during the previous 12-week period.

## North Star Checklist

1. [[What game are you playing?]] 
2. Get Familiar with [[Example outputs of a North Star workshop]]
3. Focus on [[Key Value Exchanges]]
4. Avoid [[Footguns & anti-patterns]] 
5. Think about [[Running a North Star Workshop]]