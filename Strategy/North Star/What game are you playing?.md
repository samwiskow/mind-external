#northstar #north #star #strategy #methodology

One of the [[North Star Framework]]
## Identify the Game You are Playing

- The Attention Game  
	- How much time are your customers willing to spend in your product?

- The Transaction Game 
	- How many transactions does your customers make in your product?

- The Productivity Game
	- How efficiently and effectively can someone get their work done?

#### YOU ARE PLAYING THE ATTENTION GAME...

**If you find yourself saying this:**

- Our business benefits directly if customers spend more time with our product. • We want customers’ mindshare.  
- Customers use our product to read, watch, listen, and play.

**Or your product is similar to this:**

- Facebook (social networking) • Netflix (online video)  
- Atlanta Journal  
- Constitution (newspaper)

**Example of an Attention oriented North Star Metric:**  
- Company: Atlanta Journal-Constitution (newspaper)
- North Star Metric: Regular readers  
- Definition: Number of subscribers consuming more than 7 articles per month

#### YOU ARE PLAYING THE TRANSACTION GAME...

**If you find yourself saying this:**

- Our business benefits directly if customers participate in the economy using our product.    
- We want customers’ wallets.    
- Payments and commerce are important to us.    
- Customers use our product to purchase, order, measure, transact, and track.
    
**Or your product is similar to this:**

- Amazon (retail and logistics)  
- Walmart (discount retail)  
- Progressive Insurance (personal insurance)

**Example of a Transaction oriented North Star Metric:**

- Company: Walmart (discount retailer) 
- North Star Metric: Full Carts
- Definition: Number of monthly purchases that contain over a certain number of items.

#### YOU ARE PLAYING THE PRODUCTIVITY GAME...

**If you find yourself saying this:**

- Our business benefits directly if customers efficiently accomplish tasks using our product.    
- Efficiency and achievement are important to us.
- Customers use our product to make, work, complete, configure, and build.

**Or your product is similar to this:**

- Salesforce (business & customer management software) • Adobe (design and publishing software)  
- LexisNexis (legal research)

**Example of a Productivity oriented North Star Metric:**

- Company: LexisNexis (legal research)
- North Star Metric: Actionable Searches
- Definition: Percentage of searches that return results selected by users within the first five results.