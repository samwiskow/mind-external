#northstar #north #star #example #diagram
## Input Metrics & North Star

Spotify Example:
![[spotify_example.png]]
GitLab Example:
![[gitlab_example.png]]



## Vision Statements & North Stars

### HYPOTHETICAL VISION STATEMENT 1:

“For the film aficionado, our documentary streaming service is the world’s most authoritative source of expertly curated short films, expert commentary, and informed community.”

#### What we learn from this statement:

- The product isn’t for everyone; it’s for aficionados.
- The product doesn’t endeavour to be comprehensive. A relatively small selection of films is a feature, not a bug.
- The films in the service’s library are short. “Authoritative” is a key characteristic of the service.
- The service is differentiated from competitors through quality of curation, commentary, and a community of like-minded people.

#### Questions this statement raises:

- Does the company differentiate through deep technical expertise in video streaming? 
- Communities are hard. How will the company build a community?

### HYPOTHETICAL VISION STATEMENT 2:

“For the boutique maker of bottled goods, our label-design app is the most reliable way to produce high-quality but affordable label designs that fit your manufacturing workflow while distinguishing your product on the shelf and your brand in the marketplace.”

#### What we learn from this statement:

- This product isn’t for huge brands; small, boutique customers are the target.    
- Reliability is critical.    
- Affordability is a distinguishing characteristic.    
- The labels need to fit into a manufacturing workflow.    
- The customer’s real value is how a label distinguishes its product and brand.
#### Questions this statement raises:

- Does the app need to integrate with third-party applications?        
- How is the label-design app distinct from more generic and widely available layout and graphics production tools?