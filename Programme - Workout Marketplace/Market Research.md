## Existing Solutions

### Programme

- https://apps.apple.com/us/app/programme-workout-plans/id1554594236
- https://programme.app/

Programme is a workout app that plans and progresses every workout for you – based on your progress, equipment and lifestyle. It serves a single user and provides both workouts and instructions for individual exercises

- https://find-and-update.company-information.service.gov.uk/company/13057637/filing-history

Turnover for [2022](https://s3.eu-west-2.amazonaws.com/document-api-images-live.ch.gov.uk/docs/RKOIBEtQSlv1PnuxxDXVjQDnqSJfgXi8ta4vvc_aV2o/application-pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIAWRGBDBV3NGNB2X5H%2F20240105%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20240105T112648Z&X-Amz-Expires=60&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEPH%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMiJGMEQCIAUIX%2Fx4bVWYmkYArT1StcujPTvCcrpnqLzjqe%2Fsvx%2BSAiAvEoAhgRi%2FRrgFQo34tNEnL0W9MOys3TISouSlEEYSFyrEBQiJ%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAQaDDQ0OTIyOTAzMjgyMiIMFDxxFv4abiYN%2FY6YKpgFJ%2BH6zmr33TQARzDvxuemgLUEmR8c0EQrjZbLBymWq9qhaNEY4d4Y3zt5fJ9%2Bl%2F2%2Fze0DZrKZ57nwz19%2Bt5agVmwwrUALgeEDYO0hrW08uZpp%2FrBXRw8Hl0Sta5cjrbaRe4nvCcymTVcLWrD2W8ogJC7sG2sXpcRGU2r0anXcEypZM0w1yJyw1ZNIhD4VeDqbFVHJayhb2jqR221%2Boyic1TR84gbQp2XqReASoQGV5CV1BavAFFrZtDkrfTTWifSV7DqYuYnwr9hS0S4rn5X9KKHAxC52%2BNxdTiiac2TR%2FjpyAKmJRLRt3lyzhfwQDXwPrY9Mr%2F2Hzjq2xcwq49%2Ff%2B%2BCSE4Q3x1oLo98xcBhd0DEyTcbiVXB5JFmX%2FnYbKq85cu1UXSYLCOeZjiJbIQ5NePeI5MUllsylVGc0OekXch6yiJLUjmuXCJsNobT7epslEYxLBnnXxEkm%2FmOUBele73iEs7fDoA7E1wOOfaGtBChIzzSjx%2F4p0k4MTkGmy3LAJzrGEvB8NX450EJvnZ%2F1jibGQdUrE1GKMn4vnQA%2BOnHP6kT7CTEoU1CdssuDZBGp%2F2bJPj1PDMqcpNrT1arRwTB3eczVYxsBikzv7n%2FRI%2Fs%2FJJ97puW8C3xTJB8umbdktF0ryXQzzhAH8pKcQeb6H%2BrGwFdmOmVQuhu9D%2BtARPw5OWHphK4kGpPr2My%2FQ%2FtA3XwNYxGgdHlmCj6oO1%2FpoVtNcIGNz%2Flzflg0hC4SV9Ku%2Bl3gcktDn%2FWnVMDDV3GFYrLxuR6AbaDX22veIkN1X695kvUvFEQ31Kutq58hr0cg41K0p%2FU4JB%2BMVy4K5IOgfPQDWnVXqFsDicl9k%2FyIUukFrxmkhf6kKxheZ4ihxYZKpfaNSp0FqzCx9t6sBjqyAQG%2BsBpzXJkVaqsXECaaA3Qp%2BJyMFtkXG5XFY6w%2FuD9Xyk6QH7ZUWOaMQffbHduEPbRjATGH4CiCDyMygmcEJdKuNf5ee1j%2BqkDBS4M1JckvpOL%2FztgLojW48L9u9RqgwC4UtLSgNOHOh%2FeXeAbFJiVYDtOW6m2%2BNoCIISTkoCjesqEco2zyz6fbtewZEa11BmOLIJUo%2FgRUAVlLTYDmtJUGIGqhQjnj%2Fk7rE7tJzZ9aono%3D&X-Amz-SignedHeaders=host&response-content-disposition=inline%3Bfilename%3D%2213057637_aa_2023-09-30.pdf%22&X-Amz-Signature=a3d63064d42da189c349ec8e713be4f27606f7a6472288f80a2b690a62f9a1ac) was around £5k. Only 9 reviews on the app. Company have been trading for 2 or three years.

### Train Heroic

- https://www.trainheroic.com/
- https://www.trainheroic.com/athlete/

Train Heroic seems to take both the consumer and coach perspective into account and seems like a mature solution both to the consumer personas needs as well as the coach who is selling programmes

![[Pasted image 20240105113156.png]]

Potentially half a million consumers and over 8000 creators selling programmes in 96 countries.

- https://apps.apple.com/us/app/trainheroic-strength-training/id955074569
- https://play.google.com/store/apps/details?id=com.TrainHeroic.TrainHeroic&hl=en_US

1.5k Ratings on the apple app store and over 100k downloads on the Google play store. Estimated revenue of $1.6 million with 18 employees.

- https://growjo.com/company/TrainHeroic

Seems to be owned by https://peaksware.com/ , which has a few similar looking apps and seems to be generating $8 million a year in revenue - https://rocketreach.co/peaksware-holdings-llc-profile_b567ec2ef6ad908e 