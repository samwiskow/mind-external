## Creator
The creator persona is one of the key personas that will use the app. The creator is an individual with experience of creating workout programs and content used to help people exercise and workout in a structured way.
### JTBD 
1. I want to be able to create workout programs that I can share with others to improve their fitness
2. I want to be able to sell my workout programs to users so that I can earn an income
3. I want to be able to see how many people complete my programme so:
	1. I can improve my programs

## Consumer
The Consumer persona is a key persona for the app. A consumer wants to be able to grab a programme that looks good to them and get started with minimal hassle.

### JTBD
1. Get results in the gym - be fitter, stronger and healthier. Some tailoring likely required based on individual goals
2. Search for and find workout programmes that help make working out easier
3. Track my progress so I can understand whether I am improving or not
4. Compare session performance from week to week so I can change things that aren't working
5. Get a summary of my session so I can see what I have just achieved
6. I want to understand how my hormonal cycle affects my training so I can adjust it in future